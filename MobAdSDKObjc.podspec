Pod::Spec.new do |s|
  s.name         = "MobAdSDKObjc"
  s.version      = "1.6.2"
  s.summary      = "MobAdSDK allows you to display ads for users based on their explicit theme selection"
  s.homepage     = "https://gitlab.com/imw-developers/mobad-sdk-ios"
  s.license      = { 
    :type => "Copyright", 
    :text => "Copyright 2020 i-magineworks. All Rights Reserved." 
  }
  s.author       = { 
    "i-magineworks" => "development.imw@i-magineworks.com" 
  }
  s.platform     = :ios
  s.ios.deployment_target = "10.0"
  s.source       = { 
    :http => "https://gitlab.com/imw-developers/mobad-sdk-ios-objc/-/raw/master/frameworks/MobAdSDKObjc-#{s.version}.zip" 
  }
  #s.ios.vendored_frameworks = "MobAdSDKObjc-#{s.version}/MobAdSDK.framework"
  s.ios.vendored_frameworks = "MobAdSDKObjc-#{s.version}/MobAdSDK.xcframework"
  s.frameworks = 'UIKit', 'UserNotifications', 'CallKit', 'CoreLocation'
  s.swift_version = "5.3"

  # Reference: https://github.com/CocoaPods/CocoaPods/issues/10104
  #s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  #s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
