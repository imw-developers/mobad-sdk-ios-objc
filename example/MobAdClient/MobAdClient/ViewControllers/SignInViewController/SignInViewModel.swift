//
//  SignInViewModel.swift
//  MobAdClient
//
//  Created by Marwan  on 12/17/19.
//  Copyright © 2019 Marwan Toutounji. All rights reserved.
//

import Foundation
import MobAdSDK

class SignInViewModel {
  func isValid(email: String) -> Bool {
    return true
  }
  
  func initiateUser(email: String, completion: @escaping (Bool, MobAdError?) -> Void) {
    MobAdSDK.shared.initiateUser(email: email, password: nil, countryCode: nil, languageCode: nil, completion: completion)
  }
  
  func initiateGuestUser(completion: @escaping (Bool, MobAdError?) -> Void) {
    MobAdSDK.shared.initiateUser(email: nil, password: nil, countryCode: nil, languageCode: nil, completion: completion)
  }
}
