//
//  SignInViewController.swift
//  MobAdClient
//
//  Created by Marwan  on 12/17/19.
//  Copyright © 2019 Marwan Toutounji. All rights reserved.
//

import MobAdSDK
import UIKit

protocol SignInViewControllerDelegate: class {
  func signInDidFinish(success: Bool)
}

class SignInViewController: UIViewController {
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var registerButton: UIButton!
  @IBOutlet weak var guestButton: UIButton!
  
  weak var delegate: SignInViewControllerDelegate?
  
  let viewModel = SignInViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureViewController()
  }
  
  private func configureViewController() {
    if #available(iOS 13.0, *) {
      isModalInPresentation = true
    }
    applyTheme()
    registerButton.addTarget(self, action:
      #selector(registerButtonTouchUpInside), for: .touchUpInside)
    guestButton.addTarget(self, action:
      #selector(guestButtonTouchUpInside), for: .touchUpInside)
    emailTextField.delegate = self
  }
  
  // MARK: ACTIONS
  //==============
  @objc func registerButtonTouchUpInside() {
    registerUser()
  }
  
  @objc func guestButtonTouchUpInside() {
    signInAsGuest()
  }
  
  private func registerUser() {
    guard let userEmail = emailTextField.text else {
      displayEmailEmptyAlert()
      return
    }
    guard viewModel.isValid(email: userEmail) else {
      displayEmailInvalidAlert()
      return
    }
    
    startLoader()
    viewModel.initiateUser(email: userEmail) { [weak self] (success, error) in
      guard let self = self else { return }
      self.stopLoader()
      if !success {
        self.displayFailToInitiateUserAlert(error: error)
      }
      self.delegate?.signInDidFinish(success: success)
    }
  }
  
  private func signInAsGuest() {
    startLoader()
    viewModel.initiateGuestUser { [weak self] (success, error) in
      guard let self = self else { return }
      self.stopLoader()
      if !success {
        self.displayFailToInitiateGuestUserAlert(error: error)
      }
      self.delegate?.signInDidFinish(success: success)
    }
  }
  
  // MARK: HELPERS
  //==============
  private func applyTheme() {
    guestButton.customizeAsSecondaryButton()
    registerButton.customizeAsPrimaryButton()
  }
  
  private func displayEmailEmptyAlert() {
    // TODO: To be implemented
  }
  
  private func displayEmailInvalidAlert() {
    // TODO: To be implemented
  }
  
  private func displayFailToInitiateUserAlert(error: MobAdError?) {
    // TODO: To be implemented
  }
  
  private func displayFailToInitiateGuestUserAlert(error: MobAdError?) {
    // TODO: To be implemented
  }
}

extension SignInViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
