//
//  CacheTableViewController.swift
//  MobAdClient
//
//  Created by Marwan Toutounji on 5/4/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import UIKit

class CacheTableViewController: UITableViewController {
  let viewModel = CacheTableViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    clearsSelectionOnViewWillAppear = false
    tableView.tableFooterView = UIView()
    
    let headerView = CacheHeaderView()
    headerView.heightAnchor.constraint(equalToConstant: 80).isActive = true
    headerView.setRecentSyncDate(viewModel.recentSyncDate)
    headerView.setRecentAdsFetchDate(viewModel.recentAdsFetchDate)
    tableView.tableHeaderView = headerView
    tableView.tableHeaderView?.layoutIfNeeded()
  }
  
  // MARK: - Table view data source
  //===============================
  override func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.numberOfSections
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowsInSection(section)
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let section = CacheTableViewModel.Section(rawValue: indexPath.section) else {
      return UITableViewCell()
    }
    
    let cell: UITableViewCell
    switch section {
    case .adsCache:
      cell = tableView.dequeueReusableCell(withIdentifier: "CachedAdCell", for: indexPath)
      if let info = viewModel.infoForAdRow(at: indexPath) {
        if let imageURL = info.thumbnail {
          _ = cell.imageView?.load(url: imageURL)
        }
        cell.textLabel?.text = info.title
        cell.detailTextLabel?.text = info.mediaType
      }
    case .mediaCache:
      cell = tableView.dequeueReusableCell(withIdentifier: "CachedMediaCell", for: indexPath)
      if let info = viewModel.infoForMediaRow(at: indexPath) {
        cell.textLabel?.text = info.expiryDate.string()
        cell.detailTextLabel?.text = info.name
      }
    }
    return cell
  }
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = TableViewSectionHeader()
    view.title = viewModel.titleForHeaderInSection(section)
    return view
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40
  }
  
  // MARK: - Action
  //===============
  @IBAction func donwButtonTapped() {
    self.dismiss(animated: true, completion: nil)
  }
}
