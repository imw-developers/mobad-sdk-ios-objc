//
//  CacheTableViewModel.swift
//  MobAdClient
//
//  Created by Marwan Toutounji on 5/4/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import Foundation
import MobAdSDK

class CacheTableViewModel {
  let internalViewModel = SyncViewModel()
  
  // MARK: - Table View Header View
  //===============================
  var recentSyncDate: Date? {
    internalViewModel.recentSyncDate
  }
  
  var recentAdsFetchDate: Date? {
    internalViewModel.adsForSchedulingDate
  }
  
  // MARK: - Table View Sections
  //============================
  var numberOfSections: Int {
    return 2
  }
  
  func numberOfRowsInSection(_ section: Int) -> Int {
    guard let section = Section(rawValue: section) else {
      return 0
    }
    switch section {
    case .adsCache:
      return internalViewModel.scheduledAds.numberOfRows
    case .mediaCache:
      return internalViewModel.records.numberOfRows
    }
  }
  
  func infoForRow(at indexPath: IndexPath) -> Any? {
    guard let section = Section(rawValue: indexPath.section) else {
      return nil
    }
    let row = indexPath.row
    switch section {
    case .adsCache:
      return internalViewModel.scheduledAds.infoForRow(at: row)
    case .mediaCache:
      return internalViewModel.records.infoForRow(at: row)
    }
  }
  
  func infoForAdRow(at indexPath: IndexPath) -> SyncViewModelScheduledAdRowInfo? {
    return infoForRow(at: indexPath) as? SyncViewModelScheduledAdRowInfo
  }
  
  func infoForMediaRow(at indexPath: IndexPath) -> SyncViewModelRecordRowInfo? {
    return infoForRow(at: indexPath) as? SyncViewModelRecordRowInfo
  }
  
  func titleForHeaderInSection(_ section: Int) -> String {
    guard let section = Section(rawValue: section) else {
      return "You should never see this string"
    }
    switch section {
    case .adsCache:
      return String.localized.ads
    case .mediaCache:
      return String.localized.media
    }
  }
}

// MARK: - Table View Sections Declaration
//========================================
extension CacheTableViewModel {
  enum Section: Int {
    case adsCache = 0
    case mediaCache
  }
}
