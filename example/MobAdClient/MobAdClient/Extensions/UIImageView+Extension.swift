//
//  UIImageView+Extension.swift
//  MobAdClient
//
//  Created by Marwan Toutounji on 5/4/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import UIKit

extension UIImageView {
  func load(url: URL, placeholder: UIImage? = nil, cache: URLCache? = nil, completion: (() -> Void)? = nil) -> URLSessionDataTask? {
    let cache = cache ?? URLCache.shared
    let request = URLRequest(url: url)
    var dataTask: URLSessionDataTask?
    if let data = cache.cachedResponse(for: request)?.data,
      let image = UIImage(data: data) {
      DispatchQueue.main.async {
        self.image = image
      }
      completion?()
    } else {
      DispatchQueue.main.async {
        self.image = placeholder
      }
      dataTask = URLSession.shared.dataTask(with: request, completionHandler: { [weak self] (data, response, error) in
        defer {
          completion?()
        }
        guard let self = self else { return }
        guard error != nil else { return }
        guard let data = data,
          let response = response,
          ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300,
          let image = UIImage(data: data) else {
            return
        }
        let cachedData = CachedURLResponse(response: response, data: data)
        cache.storeCachedResponse(cachedData, for: request)
        DispatchQueue.main.async {
          self.image = image
        }
      })
      DispatchQueue.global().async {
        dataTask?.resume()
      }
    }
    return dataTask
  }
}
