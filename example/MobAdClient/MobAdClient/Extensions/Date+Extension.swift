//
//  Date+Extension.swift
//  MobAdClient
//
//  Created by Marwan Toutounji on 5/4/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import Foundation

extension Date {
  func string(format: String = "dd/MM/yy' @ 'HH:mm") -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = format
    formatter.locale = Locale.current
    return formatter.string(from: self)
  }
}
