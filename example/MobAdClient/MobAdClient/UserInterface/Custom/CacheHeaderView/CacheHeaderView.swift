//
//  CacheHeaderView.swift
//  MobAdClient
//
//  Created by Marwan Toutounji on 5/4/20.
//  Copyright © 2020 Marwan Toutounji. All rights reserved.
//

import UIKit

class CacheHeaderView: UIView {
  @IBOutlet weak var contentView: UIView!
  
  @IBOutlet weak var recentSyncDateLabel: UILabel!
  @IBOutlet weak var recentSyncDateValue: UILabel!
  @IBOutlet weak var recentAdsFetchDateLabel: UILabel!
  @IBOutlet weak var recentAdsFetchDateValue: UILabel!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initialize()
  }
    
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    initialize()
  }

  private func initialize() {
    Bundle.main.loadNibNamed("CacheHeaderView", owner: self, options: nil)
    translatesAutoresizingMaskIntoConstraints = false
    
    contentView.fixInView(self)
    
    recentSyncDateLabel.text = String.localized.recentSyncDate
    recentAdsFetchDateLabel.text = String.localized.recentAdsFetchDate
    
    applyTheme()
  }
  
  func setRecentSyncDate(_ date: Date?) {
    recentSyncDateValue.text = date?.string()
  }
  
  func setRecentAdsFetchDate(_ date: Date?) {
    recentAdsFetchDateValue.text = date?.string()
  }
  
  private func applyTheme() {
    let textColor = UIColor.orange
    recentSyncDateLabel.textColor = textColor
    recentSyncDateValue.textColor = textColor
    recentAdsFetchDateLabel.textColor = textColor
    recentAdsFetchDateValue.textColor = textColor
  }
}
